#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/uaccess.h>
#include <linux/ioport.h>
#include <asm/io.h>

#define AUTHOR 	""
#define DESC   	""
#define VERSION	""
#define DRIVER "mydriver"

MODULE_LICENSE("GPL");
MODULE_AUTHOR(AUTHOR);
MODULE_DESCRIPTION(DESC);
MODULE_VERSION(VERSION);

/*Funções da Interface do Driver*/
static int __init init_mydriver(void);
static void __exit cleanup_mydriver(void);
static int mydriver_open(struct inode *i, struct file *f);
static int mydriver_close(struct inode *i, struct file *f);
static ssize_t mydriver_read(struct file *f, char __user *buf, size_t len, loff_t *off);


static struct file_operations fops = {
  .owner = THIS_MODULE,
  .open = mydriver_open,
  .release = mydriver_close,
  .read = mydriver_read,
};


static dev_t mydriver_dev;
static struct cdev mydriver_cdev;


static int __init init_mydriver(void)
{
  int code;


  /*Tenta obter um device number*/
  code = alloc_chrdev_region(&mydriver_dev, 0, 1, DRIVER);

  /*código 0: sucesso*/
  if (code) {
    printk(KERN_ALERT "Allocating device number failed! DRIVER: %s\n", DRIVER);
    goto error_alloc_chrdev_region;
  }

  /*determina as funções de callback para as operações do driver*/
  cdev_init(&mydriver_cdev, &fops);

  mydriver_cdev.owner = THIS_MODULE;
  
  /*tenta adicionar o driver no sistema - com apenas 1 minor*/
  code = cdev_add(&mydriver_cdev, mydriver_dev, 1);

  if (code) {
    printk(KERN_ALERT "Registering driver device failed!  DRIVER: %s\n", DRIVER);
    goto error_cdev_add;
  }

  printk(KERN_ALERT "Assigned major number %d! DRIVER: %s\n", MAJOR(mydriver_cdev.dev), DRIVER);

  
  return 0;


  /*error*/
error_cdev_add:
  unregister_chrdev_region(mydriver_dev, 1);
error_alloc_chrdev_region:
  return code;
}

static void __exit cleanup_mydriver(void)
{
  cdev_del(&mydriver_cdev);
  unregister_chrdev_region(mydriver_cdev.dev, 1);
  printk(KERN_ALERT "driver stopped\n");
}

static int mydriver_open(struct inode *i, struct file *f)
{
    printk(KERN_INFO "mydriver: open()\n");
    return 0;
}

static int mydriver_close(struct inode *i, struct file *f)
{
    printk(KERN_INFO "mydriver: close()\n");
    return 0;
}

static ssize_t mydriver_read(struct file *f, char __user *buf, size_t len, loff_t *off)
{
    printk(KERN_INFO "mydriver: read()\n");

    return 0;
}


module_init(init_mydriver);
module_exit(cleanup_mydriver);
