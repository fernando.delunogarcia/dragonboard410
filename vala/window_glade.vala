using Gtk;

int main (string[] args) {     
    Gtk.init (ref args);

    var builder = new Builder ();
    /* Getting the glade file */
    builder.add_from_file ("ui_design.glade");
    builder.connect_signals (null);
    var window = builder.get_object ("window1") as Window;
    window.show_all ();
    Gtk.main ();

    return 0;
}