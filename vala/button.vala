public class Application : Gtk.Window {

    private int click_counter;

	public Application () {
		this.title = "Aula SO Embarcados";
		this.window_position = Gtk.WindowPosition.CENTER;
		this.destroy.connect (Gtk.main_quit);
		this.set_default_size (350, 70);

        Gtk.Button button = new Gtk.Button.with_label ("contador");
		this.add (button);
        button.clicked.connect(BtnEvent);

        click_counter = 0;
	}

    private void BtnEvent(Gtk.Button btn){
        btn.label = "contador = %d".printf(++this.click_counter);
    }
}

public static int main (string[] args) {
		Gtk.init (ref args);

		Application app = new Application ();
		app.show_all ();
		Gtk.main ();
		return 0;
}